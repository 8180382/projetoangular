import { Component, OnInit } from '@angular/core';
import{TeamschService} from './teamsch.service';
import{LOG} from './mocks';
import { NgIf } from '@angular/common';
import{ActivatedRoute} from '@angular/router';
import{Location}  from '@angular/common';
@Component({
  selector: 'app-teamsch',
  templateUrl: './teamsch.component.html',
  styleUrls: ['./teamsch.component.css']
})
export class TeamschComponent implements OnInit {
  football:{};
title:String;
Log:{};
id:String;
  route: any;

  constructor( private teamchservice:TeamschService) { }
 
  ngOnInit() {
    
    this.getFootball();
    this.title="Football";
    this.Log=LOG;
  }

  getFootball(){
    this.teamchservice.getFootball().subscribe(football => this.football=football);
  }


  getlog(id1){
    for(let team of LOG){
      if(team.id == id1){
      return team.url;
    }

    }
    
  }
  initreturn(){
    const id = this.route.snapshot.paramMap.get('id');
    if(id != null){
      this.id=id;
    }
  }
  getequi(id1){
    for(let team of LOG){
      if(team.id == id1){
      return team.equi;
    }

    }
  }
}
 